#!/bin/sh

# Template HAProxy configuration
/template.py /template.cfg /usr/local/etc/haproxy/haproxy.cfg
echo Using Configuration
cat /usr/local/etc/haproxy/haproxy.cfg

# Start our tiny syslog collector in the background
/syslog.py &

# Start HAProxy
exec haproxy -f /usr/local/etc/haproxy/haproxy.cfg
