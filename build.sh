#!/bin/sh

# Build a container
function build_container {
    container=$1
    version=$2
    registry=$3
    # build
    pushd $container
    docker build -t "${container}:${version}" -t "${container}:latest" -t "${registry}/${container}:${version}" -t "${registry}/${container}:latest" .
    docker push "${registry}/${container}:${version}"
    docker push "${registry}/${container}:latest"
    popd
}

# Our public docker registry from gitlab
CONTAINERS_REGISTRY="registry.gitlab.com/intrbiz-open-source/containers"

# Build our containers
build_container "haproxy" "1.8" "$CONTAINERS_REGISTRY"
