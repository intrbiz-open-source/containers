# HAProxy

This is a simple HAProxy container to load balance traffic for an application across a number of other servers / containers.  Configuration is provided by environment variables provided to the container.  On container start up the HAProxy configuration file is templated using the environment variables.

The default configuration create a single load balancer over a pool of application servers, with no session stickiness.  Each server must offer a healthcheck, to be active in the balancer (by default `/` must return a HTTP 200).

## Running HAProxy

    docker create -e "app=myapp" -e "check_uri=/status" -e "members=172.17.0.3:80 172.17.0.4:80" -p 8080:80 -p 9090:9090 --name my-haproxy registry.gitlab.com/intrbiz-open-source/containers/haproxy:1.8
    
    docker start my-haproxy

The following environment variable can be provided to configure HAProxy:

* `members` The servers to load balance, separated by spaces, for example: `172.17.0.3:8080 172.17.0.4:8080 172.17.0.5:8080`
* `check_uri` The URI of the server healthcheck endpoint which will return HTTP 200 if the server is active, defaults to `/`
* `app` The application name, defaults to `myapp`
* `stats_username` The stats web interface username, defaults to `admin`
* `stats_password` The stats web interface password, defaults to `admin`

### HAProxy Stats Interface

The HAProxy stats interface is enabled by default, on port 9090, point your browser to `http://127.0.0.1:9090/`.  The default username and password is `admin`.

## Custom Configuration

If you wish to customise the configuration further, you can create a container and provide your configuration template.  The configuration is templated using Jinja2.

### DockerFile

    FROM registry.gitlab.com/intrbiz-open-source/containers/haproxy:1.8
    
    # Copy in our customised configuration
    COPY template.cfg /

### template.cfg

    # Global HAProxy configuration
    global
        log 127.0.0.1:5141 daemon info
        maxconn 8192
    
    # Defaults for all proxies
    defaults
        log               global
    
    # Application frontend
    frontend {{ app | default('myapp') }}_frontend
        bind *:80 
        mode http
        use_backend {{ app | default('myapp') }}_backend
    
    # Application backend
    backend {{ app }}_backend
        balance roundrobin
        mode http
        option forwardfor
    {% for server in members.split(' ') %}
        server server_{{ loop.index }} {{ server }}
    {% endfor %}
