#!/usr/bin/python3

import os
import sys
from jinja2 import Template

with open(sys.argv[1]) as template_file:
    with open(sys.argv[2], 'w') as template_output:
        template_output.write(Template(template_file.read()).render(os.environ))
